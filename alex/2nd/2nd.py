CUBE_LIMITS = {
  "red": 12,
  "green": 13,
  "blue": 14
}

def process_line(line):
  game_no, shown_subsets = parse_line(line)
  for shown_subset in shown_subsets:
    for color, quantity in shown_subset.items():
      if quantity > CUBE_LIMITS[color]:
        return 0
  return game_no

def process_line2(line):
  max_shown_color = {
    'red': 0,
    'green': 0,
    'blue': 0 
  }

  _, shown_subsets = parse_line(line)
  for shown_subset in shown_subsets:
    for color, quantity in shown_subset.items():
      if quantity > max_shown_color[color]:
        max_shown_color[color] = quantity
  return max_shown_color['blue']*max_shown_color['red']*max_shown_color['green']


def parse_showing(showing):
  shown_subset = {
    'red': 0,
    'green': 0,
    'blue': 0
  }
  for color_quantity in showing.split(','):
    color_quantity = color_quantity.strip().split(' ')
    shown_subset[color_quantity[1]] = int(color_quantity[0])
  return shown_subset

def parse_line(line):
  game_no = None
  shown_subsets = []
  line = line.split(":")
  game_no = int(line[0].split(' ')[1])
  for showing in line[1].split(';'):
    shown_subsets.append(parse_showing(showing))
  return game_no, shown_subsets

sum = 0

with open("input.txt", "r") as f:
  for line in f:
    sum += process_line(line)

print(f'the first result is {sum}')

sum2 = 0
with open("input.txt", "r") as f:
  for line in f:
    sum2 += process_line2(line)

print(f'the second result is {sum2}')