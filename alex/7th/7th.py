import pandas as pd

 # two indices: type rank and hand encoded as hex
 # each card in the hand will be assigned a hex digit
HANDS = pd.DataFrame()

CARD_TO_HEXDIGIT = {
  '2': '1',
  '3': '2',
  '4': '3',
  '5': '4',
  '6': '5',
  '8': '7',
  '9': '8',
  'T': '9',
  'J': 'A',
  'Q': 'B',
  'K': 'C',
  'A': 'D' 
}

with open('test.txt') as f:
  for line in f:
    parse_line(line)