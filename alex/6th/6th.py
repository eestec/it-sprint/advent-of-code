import math

races = [(48,390),(98,1103),(90,1112),(83,1360)] # (time, distance)
test_races = [(7,9),(15,40),(30,200)]

def winning_window(race_duration, high_score_distance):
  lower_bound = race_duration/2 - math.sqrt((race_duration**2)/4-high_score_distance)
  upper_bound = race_duration/2 + math.sqrt((race_duration**2)/4-high_score_distance)
  if upper_bound.is_integer():
    upper_bound -= 1
  return int(lower_bound)+1, int(upper_bound)

product = 1
for race in races:
  lower_bound, upper_bound = winning_window(*race)
  number_of_winning_ways = upper_bound-lower_bound+1
  product *= number_of_winning_ways

print(product)

# part 2
lower_bound, upper_bound = winning_window(48989083, 390110311121360)
number_of_winning_ways = upper_bound-lower_bound+1

print(number_of_winning_ways)