def winning_numbers_on_card(card):
  drawn_numbers_bet_numbers = card.split(': ')[1].split('|')
  drawn_numbers = set([int(number) for number in drawn_numbers_bet_numbers[0].strip().split(' ') if number != ''])
  bet_numbers   = set([int(number) for number in drawn_numbers_bet_numbers[1].strip().split(' ') if number != ''])
  winning_numbers_count = len(bet_numbers.intersection(drawn_numbers))
  return winning_numbers_count

def points_on_card(card):
  winning_numbers_count = winning_numbers_on_card(card)
  if winning_numbers_count == 0:
    return 0
  else:
    return 2**(winning_numbers_count-1)

test_data = {
"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53" : 8,
"Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19" : 2,
"Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1" : 2,
"Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83" : 1,
"Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36" : 0,
"Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11" : 0
}

for card, points in test_data.items():
  res = points_on_card(card)
  print(f"{card.split(':')[0]}. Test passed {res == points}. got {res}")

resulting_sum = 0
with open('input.txt', 'r') as f:
  for card in f:
    resulting_sum += winning_numbers_on_card(card)

print(resulting_sum)

CARD_MULTIPLES = [1 for _ in open('input.txt')]

print(len(CARD_MULTIPLES))

with open('input.txt', 'r') as f:
  for i, card in enumerate(f):
    winning_numbers_count = winning_numbers_on_card(card)
    for nth_winning_number in range(1, winning_numbers_count+1):
      CARD_MULTIPLES[i+nth_winning_number] += CARD_MULTIPLES[i]
      
print(CARD_MULTIPLES)

print(sum(multiple for multiple in CARD_MULTIPLES))