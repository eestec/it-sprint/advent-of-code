import numpy as np
def process_line_fancy(line):
  data = [int(number) for number in line.split()]
  p = np.polyfit(list(range(len(data))), data, len(data)-1)
  res = 0 
  for i in range(len(data)):
    res += p[-(i+1)]*len(data)**i
  return res

def process_line_std(line):
  levels = [[]]
  levels[0] = [int(number) for number in line.split()]
  i = 0
  while any(levels[i]):
    i += 1
    levels.append([right-left for left, right in zip(levels[i-1][:-1],levels[i-1][1:])])
  res = 0
  for level in reversed(levels):
    res = level[0] - res
  return res

    

summed_res = 0
with open('input.txt') as f:
  for line in f:
    summed_res += int(process_line_std(line))

print(summed_res)