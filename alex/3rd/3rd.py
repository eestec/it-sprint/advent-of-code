import string
SYMBOL_LOCATIONS = {}
POTENTIAL_PART_NUMBERS = {}
POTENTIAL_GEARS = {}

def adjacent_symbol_exists(part_number, line, column):
  # check the line of the part number, plus the two adjacent
  # symbol is adjecent, if it's colum is in [column-1, column+len(part_number)]
  found_symbol = False
  for i in (-1, 0, 1):
    check_line = line+i
    if check_line in SYMBOL_LOCATIONS.keys():
      for symbol_column in SYMBOL_LOCATIONS[check_line]:
        if symbol_column in range(column-1, column+len(part_number)+1):
          if (check_line, symbol_column) in POTENTIAL_GEARS.keys():
            POTENTIAL_GEARS[(check_line, symbol_column)] += [int(part_number)]
          found_symbol = True
  return found_symbol

# find symbols and potential part numbers
filename = 'input.txt'
# filename = 'test.txt'

with open(filename, 'r') as f:
  current_potential_part_number = {
    "column": -1,
    "value": ""
  }

  for i, line in enumerate(f):
    for column, character in enumerate(line):
      if character in string.digits:
        current_potential_part_number["value"] += character
        if current_potential_part_number["column"] == -1:
          current_potential_part_number["column"] = column
      elif current_potential_part_number["column"] != -1:
        if current_potential_part_number["value"] in POTENTIAL_PART_NUMBERS.keys():
          POTENTIAL_PART_NUMBERS[current_potential_part_number["value"]] += \
            [(i,current_potential_part_number["column"])]
        else:
          POTENTIAL_PART_NUMBERS[current_potential_part_number["value"]] = \
            [(i,current_potential_part_number["column"])]
        current_potential_part_number["column"] = -1
        current_potential_part_number["value"] = ""
      if character not in string.digits + ".\n":
        if i not in SYMBOL_LOCATIONS.keys():
          SYMBOL_LOCATIONS[i] = [column]
        else:
          SYMBOL_LOCATIONS[i].append(column)
        if character == "*":
          POTENTIAL_GEARS[(i,column)] = []

# print("-- found symbol locations --")
# print(SYMBOL_LOCATIONS)

# print("-- potential part numbers --")
# print(POTENTIAL_PART_NUMBERS)

sum = 0
for potential_part_number, locations in POTENTIAL_PART_NUMBERS.items():
  for location in locations:
    if adjacent_symbol_exists(potential_part_number, *location):
      sum += int(potential_part_number)
    # else:
    #   print(f"{potential_part_number} at {location} is not a part number.")

print(sum)

sum2 = 0

for _, ratio_factors in POTENTIAL_GEARS.items():
  if len(ratio_factors) == 2:
    sum2 += ratio_factors[0]*ratio_factors[1]
print(POTENTIAL_GEARS)
print(sum2)