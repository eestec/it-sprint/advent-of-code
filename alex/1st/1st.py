import string
sum = 0
with open("input.txt", 'r') as input:
  for line in input:
    numbers_str = "".join(c for c in line if c in string.digits)
    sum += int(numbers_str[0]+numbers_str[-1])

# print(f"first solution is {sum}")

sum2 = 0
DIGIT_WORDS = {
  "one": "1",
  "two": "2",
  "three": "3",
  "four": "4",
  "five": "5",
  "six": "6",
  "seven": "7",
  "eight": "8",
  "nine": "9"
}

DIGIT_WORDS_REVERSED = {
  "eno": "1",
  "owt": "2",
  "eerht": "3",
  "ruof": "4",
  "evif": "5",
  "xis": "6",
  "neves": "7",
  "thgie": "8",
  "enin": "9"
}

def process_line_alt(line):
  # find first number
  first_number=""
  for i in range(0,len(line)):
    character = line[i]
    if character in string.digits:
      first_number = character
      break
    if character == "o":
      # check for one
      if line[i:i+3] == "one":
        first_number = "1"
        break
    if character == "t":
      # check for two or three
      if line[i:i+3] == "two":
        first_number = "2"
        break
      elif line[i:i+5] == "three":
        first_number = "3"
        break
    if character == "f":
      # check for four or five
      if line[i:i+4] in ("four", "five"):
        first_number = DIGIT_WORDS[line[i:i+4]]
        break
    if character == "s":
      # check for six or seven
      if line[i:i+3] == "six":
        first_number = "6"
        break
      elif line[i:i+5] == "seven":
        first_number = "7"
        break
    if character == "e":
      # check for eight
      if line[i:i+5] == "eight":
        first_number = "8"
        break
    if character == "n":
      # check for nine
      if line[i:i+4] == "nine":
        first_number = "9"
        break

  # find last number
  line_reversed = line[::-1]
  last_number = ""

  for i in range(0,len(line)):
    character = line_reversed[i]
    if character in string.digits:
      last_number = character
      break
    if character == "e":
      # check for 1, 3, 5 and 9
      if line_reversed[i:i+3] == "eno":
        last_number = "1"
        break
      elif line_reversed[i:i+4] in ("evif", "enin"):
        last_number = DIGIT_WORDS_REVERSED[line_reversed[i:i+4]]
        break
      elif line_reversed[i:i+5] == "eerht":
        last_number = "3"
        break
    if character == "o":
      # check for 2
      if line_reversed[i:i+3] == "owt":
        last_number = "2"
        break
    if character == "r":
      # check for 4
      if line_reversed[i:i+4] == "ruof":
          last_number = "4"
          break
    if character == "x":
      # check for 6
      if line_reversed[i:i+3] == "xis":
        last_number = "6"
        break
    if character == "n":
      # check for 7
      if line_reversed[i:i+5] == "neves":
          last_number = "7"
          break
    if character == "t":
      # check for 8
      if line_reversed[i:i+5] == "thgie":
          last_number = "8"
          break
      
  return int(first_number+last_number)

def process_line(line):
  line_no_digit_words = line
  # replace digit words
  found_digit_words = {}
  for digit_word in DIGIT_WORDS.keys():
    index = line.find(digit_word)
    if index != -1:
      found_digit_words[index] = digit_word

  print(found_digit_words)
  if len(found_digit_words.keys()) > 0:
    indexes = list(found_digit_words.keys())
    indexes.sort()
    
    if len("".join(c for c in line_no_digit_words[:indexes[0]] if c in string.digits)) == 0:
      first_digit_word = found_digit_words[indexes[0]]
      line_no_digit_words = line_no_digit_words.replace(first_digit_word, DIGIT_WORDS[first_digit_word],1)

    if len(found_digit_words.keys()) > 1:
      last_digit_word = found_digit_words[indexes[-1]]
      line_no_digit_words = line_no_digit_words.replace(last_digit_word, DIGIT_WORDS[last_digit_word],1)

  # get all digits
  digit_str = "".join(c for c in line_no_digit_words if c in string.digits)
  return int(digit_str[0]+digit_str[-1])
  

with open("input.txt", 'r') as input:
  for line in input:
  #   res_ini = process_line(line)
    res_alt = process_line_alt(line)
    # if res_ini != res_alt:
    #   print(f"the line is {line.strip()}. Initial solution is {res_ini} and the alternative is {res_alt}")
    # if res_alt != int(line[-1]):
    #   print(f"With {line[0]} you got {line[-1]}, I got {res_alt}")
    sum2 += res_alt

print(f"second solution is {sum2}")

TEST_DATA = {
#  "two1nine": 29,
#   "eightwothree": 83,
#   "abcone2threexyz": 13,
#   "xtwone3four": 24,
#   "4nineeightseven2": 42,
#   "zoneight234": 14,
#   "7pqrstsixteen": 76,
#   "eightfourtwone": 81,
  "trknlxnv43zxlrqjtwonect": 41,
  "one32fourfivelkrczztone": 11
}

# print("testing initial solution")
# for line, number in TEST_DATA.items():
#   res = process_line(line)
#   print(f"Passed: { res == number}. process line test: '{line}' returns {res} ")