DIRECTIONS = ""
NODES = {}

FULL_CYCLES = {

}

with open('input.txt') as f:
  DIRECTIONS = next(f).strip()
  next(f)
  for line in f:
    line = line.strip()
    node = line.split(' = ')
    edges = node[1][1:-1].split(', ')
    NODES[node[0]] = (
      edges[0], edges[1]
    )
    FULL_CYCLES[node[0]] = [set(),""]

no_steps = 0
# while not all([node[-1] == 'Z' for node in cur_nodes]):
#   go_right = DIRECTIONS[no_steps% len(DIRECTIONS)] == 'R'
#   cur_nodes = [NODES[node][go_right] for node in cur_nodes]
#   no_steps += 1
#   if not no_steps % 1000:
#     print(no_steps)


for key in NODES.keys():
  cur_node = key
  for i, c in enumerate(DIRECTIONS):
    go_right = c == "R"
    cur_node = NODES[cur_node][go_right]
    if cur_node[-1] == 'Z':
      FULL_CYCLES[cur_node][0].add(i+1)
  FULL_CYCLES[key][1] = cur_node

cur_nodes  = [node for node in NODES.keys() if node[-1] == 'A']


print(FULL_CYCLES)
passthroughs = 0
common_end_node_indices = set()
while not len(common_end_node_indices) > 0:
  common_end_node_indices = FULL_CYCLES[cur_nodes[0]][0].intersection(*[FULL_CYCLES[node][0] for node in cur_nodes[1:]]) 
  cur_nodes = [FULL_CYCLES[node][1] for node in cur_nodes]
  passthroughs += 1
  if not passthroughs % 1000000:
    print(passthroughs)


no_steps = min(common_end_node_indices) + (passthroughs-2) * len(DIRECTIONS)

print(no_steps)