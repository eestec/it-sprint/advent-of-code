import numpy as np
from tqdm import tqdm


MAPS = {
"seeds": [],
"seed-to-soil map": [], 
"soil-to-fertilizer map": [],
"fertilizer-to-water map": [],
"water-to-light map": [],
"light-to-temperature map": [],
"temperature-to-humidity map": [],
"humidity-to-location map": []
}

MAP_ORDER = [
"seed-to-soil map", 
"soil-to-fertilizer map",
"fertilizer-to-water map",
"water-to-light map",
"light-to-temperature map",
"temperature-to-humidity map",
"humidity-to-location map"]

with open('input.txt', 'r') as f:
  mode = ""
  for line in f:
    if ":" in line:
      # new section of input
      mode = line.split(":")[0]
      if mode == "seeds":
        # seeds is the only mode tag that is on the same line with the data
         MAPS[mode] +=[int(i) for i in line.split(":")[1].strip().split(' ')]
    elif line != "\n":
      MAPS[mode].append([int(i) for i in line.strip().split(' ')])

LOCATIONS = []

def merge_maps(map1, map2):
  merged_map = []
  def _follow_forward(line, lower_bound):
    new_range = [0,lower_bound,0]
    new_range[0], map2_rule = value_lookup(line[0], map2)[0]
    if map2_rule:
      if map2_rule[0]+map2_rule[2] >= new_range[0]+line[2]:
        new_range[2] = line[2]
        merged_map.append(new_range)
      else:
        new_range[2] = map2_rule[0]+map2_rule[2] - new_range[1]
        merged_map.append(new_range)
        _follow_forward(line, new_range[1]+new_range[2])
    else:
      

  def _follow_backward(line, lower_bound):
    pass

  for line in map1:
    _follow_forward(line, line[1])
  for line in map2:
    _follow_backward(line, line[0])

  return merged_map


def value_lookup(input, lookup_map):
  for line in lookup_map:
    if input >= line[1] and input < line[1]+line[2]:
      return line[0]+input-line[1], line
  return input, []

for i in range(int(len(MAPS['seeds'])/2)):
  print(f"checking range {i} of {int(len(MAPS['seeds'])/2)}")
  for j in tqdm(range(MAPS['seeds'][2*i+1])):
    cur_value = MAPS['seeds'][2*i]+j
    cur_value, _ = value_lookup(cur_value, MAPS["seed-to-soil map"])

    LOCATIONS.append(cur_value)

print(min(LOCATIONS))
  


