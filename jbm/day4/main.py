from collections import defaultdict

# Constants
filename = "input.txt"

def check_win(win,my,):
    win = win.split()
    my = my.split()
    winnings = 0
    for i in my:
        if i in win:
            if winnings < 1:
                winnings += 1
            else:
                winnings *= 2
    # print(winnings)
    return winnings

def Part1():
    with open(filename) as f:
        # Constants
        lines = f.read().splitlines()
        ans = 0
        for line in lines:
            winning_num, my_num = (line[line.index(":") + 1:]).split("|")
            ans += check_win(winning_num,my_num)
            
    return ans

def check_win_pt2(win,my):
    win = win.split()
    my = my.split()
    winnings = 0
    for i in my:
        if i in win:
            winnings += 1
    # print(winnings)
    return winnings

def count_cards(scratches):
    num_cards = []
    for j in range(0,len(scratches)):
        num_cards.append(1)
    for i in scratches:
        card_num = i[0]
        win_num = i[1]
        # print(win_num)
        if win_num>0:
            for j in range(1,win_num+1):
                num_cards[card_num + j] += 1 * num_cards[card_num]
        # print(num_cards)
    return num_cards
        
    

def Part2():
    with open(filename) as f:
        # Constants
        lines = f.read().splitlines()
        ans = []
        scratch_win = []
        for i, line in enumerate(lines):
            winning_num, my_num = (line[line.index(":") + 1:]).split("|")
            scratch_win.append([i, check_win_pt2(winning_num,my_num)])
    
    ans = sum(count_cards(scratch_win))
    
    return (ans)

print(Part1())
print(Part2())
