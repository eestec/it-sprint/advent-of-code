# Imports
import math

# Constants
filename = "input.txt"

def Part1():
    ans = 0
    with open(filename) as f:
        races = [map(int, line.split()[1:]) for line in f]
        ans = 1
    for time, dist in zip(*races):
        ans *= sum((time - hold) * hold >= dist for hold in range(time))
    return ans

def Part2():
    with open(filename) as f:
        time, dist = [int(line.replace(" ", "").split(":")[1]) for line in f]
        a = (time - math.sqrt(time**2 - 4 * dist)) / 2
        b = (time + math.sqrt(time**2 - 4 * dist)) / 2
    return math.floor(b) - math.ceil(a) + 1


print(Part1())
print(Part2())
