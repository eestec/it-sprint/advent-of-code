from itertools import cycle, pairwise


# Constants
filename = "input.txt"

def seq(ints):
    if all(ints == 0 for ints in ints):
        return 0
    diffs = [b - a for a, b in pairwise(ints)]
    return ints[-1] + seq(diffs)



def part1():
    with open(filename) as f:
        return sum(seq([int(x) for x in line.split()]) for line in f)


def part2():
    with open(filename) as f:
        return sum(seq([int(x) for x in line.split()[::-1]]) for line in f)

print(part1())
print(part2())